<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use Bavix\Wallet\Traits\HasWallet;
use Bavix\Wallet\Traits\HasWallets;
use Bavix\Wallet\Traits\HasWalletFloat;
use Bavix\Wallet\Traits\CanConfirm;
use Bavix\Wallet\Interfaces\Wallet;
use Bavix\Wallet\Interfaces\WalletFloat;
use Bavix\Wallet\Interfaces\Confirmable;

use BeyondCode\Vouchers\Traits\CanRedeemVouchers;
use BeyondCode\Vouchers\Traits\HasVouchers;


class DataProfiles extends Model implements Wallet, WalletFloat, Confirmable
{
    use HasFactory, HasWallet, HasWallets, HasWalletFloat, CanConfirm, CanRedeemVouchers;

    public $fillable =  [ 'uuid', 'name', 'birthday', 'gender', 'address', 'latitude', 'longitude', 'phone_principal', 'phone_secondary', 'email_principal', 'email_secondary', 'instagram', 'facebook', 'twitter', 'youtube', 'advertising', 'created_at'];
    
    public static function GetByID($id)
    {
        $info   =   DataProfiles::where('profile', '=', $id)->first();

        if(is_null($info))
        {
            return false;
        }else{
            return [
                'profile'           =>  $info->profile,
                'name'              =>  $info->name,
                'birthday'          =>  $info->birthday,
                'gender'            =>  $info->gender,
                'address'           =>  $info->address,
                'latitude'          =>  $info->latitude,
                'longitude'         =>  $info->longitude,
                'phone_principal'   =>  $info->phone_principal,
                'phone_secondary'   =>  $info->phone_secondary,
                'email_principal'   =>  $info->email_principal,
                'email_secondary'   =>  $info->email_secondary,
                'instagram'         =>  $info->instagram,
                'facebook'          =>  $info->facebook,
                'twitter'           =>  $info->twitter,
                'youtube'           =>  $info->youtube,
                'advertising'       =>  $info->advertising,

            ];
        }


    }
}
