<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Srmklive\PayPal\Services\PayPal as PayPalClient;

class PaypalController extends Controller
{

    public function createTransaction()
    {
        return view('pages.paypal');
    }

    /**
     * Process Transaction Functions
     */
    public function processTransaction(Request $request)
    {
        $provider       =   new PayPalClient;

        $provider->setApiCredentials(config('paypal'));
        $paypalToken    =   $provider->getAccessToken();
        $response       =   $provider->createOrder([
            "intent" => "CAPTURE",
            "application_context" => [
                "return_url"    =>  route('successTransaction'),
                "cancel_url"    =>  route('cancelTransaction'),
            ],
            "purchase_units" => [
                0 => [
                    "amount" => [
                        "currency_code" => "USD",
                        "value" => "1.00"
                    ]
                ]
            ]
        ]);

        // dd($response['id'], $response['links']);

        if (isset($response['id']) && $response['id'] != null)
        {
            foreach ($response['links'] as $links) 
            {
                if($links['rel'] == 'approve')
                {
                    return redirect()->away($links['href']);
                }

            }

            return redirect()
                ->route('createTransaction')
                ->with('error', $response['message'] ?? 'Something went wrong.');
        }
    }

    /**
     * Success Transaction Functions
     */
    public function successTransaction(Request $request)
    {
        $provider   =   new PayPalClient();
        $provider->setApiCredentials(config('paypal'));
        $provider->getAccessToken();
        $response   =   $provider->capturePaymentOrder($request['token']);
        dd($response);

        if (isset($response['status']) && $response['status'] == 'COMPLETED') 
        {
            return redirect()
                ->route('createTransaction')
                ->with('success', 'Transaction complete.');
        } else {
            return redirect()
                ->route('createTransaction')
                ->with('error', $response['message'] ?? 'Something went wrong.');
        }
    }

    /**
     * Cancel Transaction Functions
     */
    public function cancelTransaction(Request $request)
    {
        return redirect()
            ->route('createTransaction')
            ->with('error', $response['message'] ?? 'You have canceled the transaction.');
    }

}
