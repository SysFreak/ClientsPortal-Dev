<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\DataProfiles;
use Illuminate\Http\Request;

use BeyondCode\Vouchers\Events\VoucherRedeemed;

class CliWalletsController extends Controller
{
    public static function view(Request $request)
    {
        return view('pages/clients/wallet/index');
    }

    public static function testWallet(Request $request)
    {
        dd("Wallet Controller - Test Wallet");
        // $clients    =   DataProfiles::first();    
        // dd($clients);    
        
        /**
         * Create Wallet for single client
         */

        // $clients->createWallet([
        //     'name'          =>  $clients->profile.'-'.$clients->mikrowisp,
        //     'slug'          =>  $clients->profile,
        //     'meta'          =>  'USD',
        //     'description'   =>  'Wallet Client: '.$clients->name.' - Mikrowisp: '.$clients->mikrowisp.'',
        // ]);

        /**
         * Get Wallet and make a deposit
         */

        // $myWallet   =   $clients->getWallet($clients->profile);
        // $myWallet->depositFloat(5.24);

        /**
         * Get Balance from Wallet
         */
        // $myWallet   =   $clients->getWallet($clients->profile);
        // $balance    =   $myWallet->balanceFloat;
        // dd($balance);

        /**
         * Discount Balance from Wallet
         */

        // $myWallet   =   $clients->getWallet($clients->profile);
        // $myWallet->WithdrawFloat(2.88, ['Description' => 'PAYMENT FOR MONTH']);
        // $balance    =   $myWallet->balanceFloat;
        // dd($balance);

        /**
         * Create Deposit for confirm transacction
         */

        // $myWallet       =   $clients->getWallet($clients->profile);
        // $transaction    =   $myWallet->depositFloat(2.5, ['deposito'], false);
        // $transaction->confirmed;
        // $myWallet       =   $clients->getWallet($clients->profile);
        // dd($myWallet->balanceFloat);

        /**
         * Confirm Payment transacction Deposit
         */
        // $myWallet       =   $clients->getWallet($clients->profile);
        // $transaction    =   $clients->transactions()->where('confirmed',0)->get();        
        // if($transaction->count() > 0)
        // {
        //     foreach ($transaction as $t => $trans) 
        //     {
        //         $myWallet->confirm($trans);
        //         $myWallet->confirmed;
        //     }
        // }

        // dd($myWallet->balanceFloat);
    }

    public static function testVoucher(Request $request)
    {
        // dd("Wallet Controller - Test Voucher");
        $voucher    =   DataProfiles::first(); 
        
        /**
         * Create Voucher for client
         */
        // $singleVoucher  =   $voucher->createVoucher(['cost' => '5.00'], today()->addDays(7));
        // dd($singleVoucher);

        /**
         * Create Multiples Voucher for clients
         */
        // $cant   =   2;
        // $singleVoucher  =   $voucher->createVouchers($cant,['cost' => '7.50'], today()->addDays(1));
        // dd($singleVoucher);

        /**
         * Validate Vaucher
         */
        $voucher    =   false;
        $code       =   'LK7V-YGS9';

        try {
            $client     =   DataProfiles::first();
            // dd($client->vouchers());
            $voucher    =   $client->redeemCode($code);
            dd($voucher);
        } catch (\Exception $ex) {
            dd($ex->getMessage());
        }

        
    }
}
