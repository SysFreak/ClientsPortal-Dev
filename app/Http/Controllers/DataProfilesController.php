<?php

namespace App\Http\Controllers;

use App\Models\DataProfiles;
use Illuminate\Http\Request;



class DataProfilesController extends Controller
{
    public static function view(Request $request)
    {
        // dd(DataProfiles::GetByID($request->id));
        return view('pages/clients/profile/index', ['data' => DataProfiles::GetByID($request->id)]);
    }
}
