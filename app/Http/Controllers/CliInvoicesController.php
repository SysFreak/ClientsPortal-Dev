<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CliInvoicesController extends Controller
{
    public static function view(Request $request)
    {
        return view('pages/clients/wallet/index');
    }

    public static function viewRegister(Request $request)
    {
        return view('pages/clients/wallet/registro');
    }
}
