<?php

namespace App\Http\Controllers;

use App\Models\ClientDashboard;
use Illuminate\Http\Request;



class ClientDashboardController extends Controller
{
    public static function view(Request $request)
    {
        return view('pages/clients/dashboard/index');
    }
}
