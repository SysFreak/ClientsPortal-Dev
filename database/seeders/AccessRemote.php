<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;

use Carbon\Carbon;

class AccessRemote extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('access_remote')->insert([
            'uuid'          =>  \Ramsey\Uuid\Uuid::uuid4()->toString(), 
            'name'          =>  "23.190.0.50", 
            'status_id'     =>  "1", 
            'created_at'    =>  Carbon::now()->format('Y-m-d H:i:s'), 
            'updated_at'    =>  Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('access_remote')->insert([
            'uuid'          =>  \Ramsey\Uuid\Uuid::uuid4()->toString(), 
            'name'          =>  "23.190.0.33", 
            'status_id'     =>  "1", 
            'created_at'    =>  Carbon::now()->format('Y-m-d H:i:s'), 
            'updated_at'    =>  Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('access_remote')->insert([
            'uuid'          =>  \Ramsey\Uuid\Uuid::uuid4()->toString(), 
            'name'          =>  "192.92.193.214", 
            'status_id'     =>  "1", 
            'created_at'    =>  Carbon::now()->format('Y-m-d H:i:s'), 
            'updated_at'    =>  Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('access_remote')->insert([
            'uuid'          =>  \Ramsey\Uuid\Uuid::uuid4()->toString(), 
            'name'          =>  "23.190.0.70", 
            'status_id'     =>  "1", 
            'created_at'    =>  Carbon::now()->format('Y-m-d H:i:s'), 
            'updated_at'    =>  Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('access_remote')->insert([
            'uuid'          =>  \Ramsey\Uuid\Uuid::uuid4()->toString(), 
            'name'          =>  "192.203.0.139", 
            'status_id'     =>  "1", 
            'created_at'    =>  Carbon::now()->format('Y-m-d H:i:s'), 
            'updated_at'    =>  Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('access_remote')->insert([
            'uuid'          =>  \Ramsey\Uuid\Uuid::uuid4()->toString(), 
            'name'          =>  "23.190.0.66", 
            'status_id'     =>  "1", 
            'created_at'    =>  Carbon::now()->format('Y-m-d H:i:s'), 
            'updated_at'    =>  Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('access_remote')->insert([
            'uuid'          =>  \Ramsey\Uuid\Uuid::uuid4()->toString(), 
            'name'          =>  "23.190.0.37", 
            'status_id'     =>  "1", 
            'created_at'    =>  Carbon::now()->format('Y-m-d H:i:s'), 
            'updated_at'    =>  Carbon::now()->format('Y-m-d H:i:s')
        ]);
    }
}
