<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;

use Carbon\Carbon;

class DataProfiles extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    { 

        DB::table('data_profiles')->insert([
            'uuid'              =>  \Ramsey\Uuid\Uuid::uuid4()->toString(),
            'mikrowisp'         =>  '16',
            'profile'           =>   sprintf("%06d", mt_rand(1, 999999)),
            'name'              =>  "Luis Campos", 
            'birthday'          =>  "2022-11-19", 
            'gender'            =>  "M", 
            'address'           =>  "Av. Libertador Centro Metropolitano Javier", 
            'latitude'          =>  "10.070162982472592", 
            'longitude'         =>  "-69.34990885938568", 
            'phone_principal'   =>  "04141234567", 
            'phone_secondary'   =>  "04261234567", 
            'email_principal'   =>  "luis.924@boomsolutions.com", 
            'email_secondary'   =>  "jesus.901@boomsolutions.com", 
            'instagram'         =>  "WebDevelopment", 
            'facebook'          =>  "WebDevelopment", 
            'twitter'           =>  "WebDevelopment", 
            'youtube'           =>  "WebDevelopment", 
            'advertising'       =>  "S", 
            'created_at'        =>  Carbon::now()->format('Y-m-d H:i:s'), 
            'updated_at'        =>  Carbon::now()->format('Y-m-d H:i:s')
        ]);
    }
}