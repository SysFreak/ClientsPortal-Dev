<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDataProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('data_profiles', function (Blueprint $table) {
            $table->id();
            $table->foreignUuid('uuid');
            $table->integer('mikrowisp')->default('16');
            $table->integer('profile')->default('000000');
            $table->string('name');
            $table->string('birthday', 10);
            $table->string('gender', 1)->default('M');
            $table->text('address');
            $table->string('latitude', 20)->default('00.000000000000000');
            $table->string('longitude', 20)->default('-00.000000000000000');
            $table->string('phone_principal',20);
            $table->string('phone_secondary',20);
            $table->string('email_principal');
            $table->string('email_secondary');
            $table->string('instagram');
            $table->string('facebook');
            $table->string('twitter');
            $table->string('youtube');
            $table->string('advertising', 1)->default('S');            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('_data_profiles');
    }
}
