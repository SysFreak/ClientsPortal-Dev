<?php

use App\Http\Controllers\ClientDashboardController;
use App\Http\Controllers\CliInvoicesController;
use App\Http\Controllers\CliWalletsController;
use App\Http\Controllers\DataProfilesController;
use App\Http\Controllers\PaypalController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('pages/clients/dashboard/index');
});

// Route::get('/clients/dashboard', function () {
//     return view('pages/clients/dashboard/index');
// });

// Route::get('/clients/profile', function () {
//     return view('pages/clients/profile/index');
// });

// Route::get('/clients/wallet', function () {
//     return view('pages/clients/wallet/index');
// });

// Route::get('/clients/wallet/registro', function () {
//     return view('pages/clients/wallet/registro');
// });

// Route::get('/clients/emails', function () {
//     return view('pages/clients/emails/index');
// });

// Route::controller(DataProfilesController::class)->group(function () {
//     Route::get('/clients/profile', 'show');
// });

Route::group(['prefix'=>'/clients'], function()
{
    Route::get('/profile/{id}',                     [DataProfilesController::class, 'view'])->name('/profile');
    Route::get('/dashboard',                        [ClientDashboardController::class, 'view'])->name('/dashboard');
    Route::get('/invoices',                         [CliInvoicesController::class, 'view'])->name('/invoices');
    Route::get('/wallet',                           [CliWalletsController::class, 'view'])->name('/wallet');
    Route::get('/wallet/register',                  [CliInvoicesController::class, 'viewRegister'])->name('/wallet/register');
    Route::get('/wallet/test/wallet',               [CliWalletsController::class, 'testWallet'])->name('/wallet/test/wallet');
    Route::get('/wallet/test/voucher',              [CliWalletsController::class, 'testVoucher'])->name('/wallet/test/voucher');
});

Route::get('create-transaction',    [PayPalController::class, 'createTransaction'])->name('createTransaction');
Route::get('process-transaction',   [PayPalController::class, 'processTransaction'])->name('processTransaction');
Route::get('success-transaction',   [PayPalController::class, 'successTransaction'])->name('successTransaction');
Route::get('cancel-transaction',    [PayPalController::class, 'cancelTransaction'])->name('cancelTransaction');